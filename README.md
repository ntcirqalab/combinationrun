# QA Lab-2 Combination Run #

### 0. Index ###

1. Overview of End-to-End Run and Combination Run
2. Question Analysis Results XML Format
3. IR Results XML Format

### 1. Overview of End-to-End Run and Combination Run ###
![Flowchart](qalab_flow.png "Flowchart")

You can submit RS results by the following two ways:

* receiving a question XML
* receiving a QA result

By starting the combination run, three types of results, which are QA, RS and FA, will be submitted.
You can perform the combination run by the following three ways:

* receiving a QA result and submitting an FA result
* receiving an RS result and submitting an FA result
* receiving several FA results and submitting an FA result

### 2. Question Analysis Results XML Format ###

Question Analysis Results XML Format consists of three XML files:

* question_analysis_result.xml
* answer_type_set_def.xml
* question_format_type_set_def.xml

#### question_anaysis_result.xml ####
An example of "quesion_analysis_result.xml" is below.
```
#!xml
<?xml version="1.0" encoding="UTF-8"?>
<TOPIC_SET>
  <METADATA>
    <RUN_ID>TEAMX-EN-JA-01-T</RUN_ID>
    <DESCRIPTION><![CDATA[We used Support Vector Machine for answer type classification and NP chunking.]]></DESCRIPTION>
    <ANSWER_TYPE_SET_DEF>
      <PATH>answer_type_set_def.xml</PATH>
    </ANSWER_TYPE_SET_DEF>
    <QUESTION_FORMAT_TYPE_DEF>
      <PATH>question_format_type_set_def.xml</PATH>
    </QUESTION_FORMAT_TYPE_DEF>
  </METADATA>
  
  <TOPIC ID="A1">
    <QUESTION_ANALYSIS>
      <QUESTION_FORMAT_TYPE_SET>
        <QUESTION_FORMAT_TYPE RANK="1" SCORE="1.0">T-SF-C</QUESTION_FORMAT_TYPE>
      </QUESTION_FORMAT_TYPE_SET>
      <ANSWER_TYPE_SET>
        <ANSWER_TYPE RANK="1" SCORE="1.0">DEFINITION</ANSWER_TYPE>
      </ANSWER_TYPE_SET>
      <QUERY_SET>
        <QUERY ID="1">
          <KEY_TERM_SET LANGUAGE="JA">
            <KEY_TERM RANK="1" SCORE="1.0">ファタハ</KEY_TERM>
            <KEY_TERM RANK="2" SCORE="0.1">組織</KEY_TERM>
          </KEY_TERM_SET>
        </QUERY>
        <QUERY ID="2">
          <KEY_TERM_SET LANGUAGE="JA">
            <KEY_TERM RANK="1" SCORE="1.0">マフムード・アッバース</KEY_TERM>
            <KEY_TERM RANK="2" SCORE="0.1">イスラエル</KEY_TERM>
          </KEY_TERM_SET>
        </QUERY>
      </QUERY_SET>
    </QUESTION_ANALYSIS>
  </TOPIC>
</TOPIC_SET>
```

* DESCRIPTION: Please write a description in English.
* RANK attribute has an integer of 1 or more. Two nodes which have the same parent node are not allowed to have the same rank.
* SCORE attribute has any value between and including 0 and 1, higher is better.
* ANSWER_TYPE_SET node has up to 5 ANSWER_TYPE nodes.
* QUESTION_FORMAT_TYPE_SET node has up to 5 QUESTION_FORMAT_TYPE nodes.
* QUERY_SET node has up to 10 QUERY nodes.
* KEY_TERM_SET node has up to 20 KEY_TERM nodes.
* LANGUAGE attribute can have an "EN" or a "JA".

#### answer_type_set_def.xml ####
An example of "answer_type_set_def.xml" is below.
```
#!xml
<?xml version="1.0" encoding="UTF-8"?>
<ANSWER_TYPE_SET_DEF>
  <ANSWER_TYPE_DEF PARENT="ROOT" CHILDREN="$">DEFINITION</ANSWER_TYPE_DEF>
  <ANSWER_TYPE_DEF PARENT="ROOT" CHILDREN="$">PERSON</ANSWER_TYPE_DEF>
  <ANSWER_TYPE_DEF PARENT="ROOT" CHILDREN="$">LOCATION</ANSWER_TYPE_DEF>
</ANSWER_TYPE_SET_DEF>
```

* PARENT attribute has any single value among other answer types or a "ROOT".
* CHILDREN attribute has multiple values among other answer types or a "$" which means an end point.

#### question_format_type_set_def.xml ####
"question_format_type_set_def.xml" is fixed, so please use the following XML. If you have any question or want to use another one which you make, please email us (qalab-admin [at] nii.ac.jp).

"question_format_type_set_def.xml"
```
#!xml
<?xml version="1.0" encoding="UTF-8"?>
<QUESTION_FORMAT_TYPE_SET_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="ROOT" CHILDREN="E-C" LABEL="Essay">E</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="E" CHILDREN="E-C-K" LABEL="Complex Essay">E-C</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="E-C" CHILDREN="E-C-K-TR,E-C-K-T" LABEL="Complex Essay with Keyword">E-C-K</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="E-C-K" CHILDREN="$" LABEL="Complex Essay by Time Period and Region with Keyword">E-C-K-TR</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="E-C-K" CHILDREN="$" LABEL="Complex Essay by Topic with Keyword">E-C-K-T</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="E-C" CHILDREN="$" LABEL="Complex Essay without Keyword">E-C-N</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="E" CHILDREN="$" LABEL="Simple Essay">E-S</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="ROOT" CHILDREN="T-F" LABEL="Term">T</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="T" CHILDREN="T-F-C" LABEL="Factoid">T-F</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="T-F" CHILDREN="T-F-C-T,T-F-C-F" LABEL="Multiple-Choice Factoid">T-F-C</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="T-F-C" CHILDREN="$" LABEL="Multiple-Choice True Factoid">T-F-C-T</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="T-F-C" CHILDREN="$" LABEL="Multiple-Choice False Factoid">T-F-C-F</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="T-F" CHILDREN="$" LABEL="Factoid with Limitation">T-F-L</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="T-F" CHILDREN="$" LABEL="Factoid without Limitation">T-F-N</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="T" CHILDREN="T-SF-C,T-SF-D" LABEL="Slot-Filing">T-SF</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="T-SF" CHILDREN="$" LABEL="Multiple-Choice Slot-Filling">T-SF-C</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="T-SF" CHILDREN="$" LABEL="Descriptive Slot-Filling">T-SF-D</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="ROOT" CHILDREN="TF-R" LABEL="True-or-False">TF</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="TF" CHILDREN="TF-R-T" LABEL="Relative True-or-False">TF-R</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="TF-R" CHILDREN="TF-R-T-F,TF-R-T-N" LABEL="Relative True">TF-R-T</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="TF-R-T" CHILDREN="$" LABEL="Relative True in Focus Word">TF-R-T-F</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="TF-R-T" CHILDREN="$" LABEL="Relative True in Whole Sentence">TF-R-T-N</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="TF-R" CHILDREN="TF-R-F-F,TF-R-F-N" LABEL="Relative False">TF-R-F</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="TF-R-F" CHILDREN="$" LABEL="Relative False in Focus Word">TF-R-F-F</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="TF-R-F" CHILDREN="$" LABEL="Relative False in Whole Sentence">TF-R-F-N</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="TF" CHILDREN="$" LABEL="Absolute True-or-False">TF-A</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="ROOT" CHILDREN="U-I" LABEL="Unique">U</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="U" CHILDREN="U-I-M,U-I-G,U-I-P" LABEL="Unique Image">U-I</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="U-I" CHILDREN="$" LABEL="Unique Map">U-I-M</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="U-I" CHILDREN="$" LABEL="Unique Graph">U-I-G</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="U-I" CHILDREN="$" LABEL="Unique Picture">U-I-P</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="U" CHILDREN="U-T-R,U-T-W" LABEL="Unique Time">U-T</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="U-T" CHILDREN="$" LABEL="Unique Time Reordering">U-T-R</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="U-T" CHILDREN="$" LABEL="Unique What Time">U-T-W</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="U" CHILDREN="$" LABEL="Unique Mixed">U-M</QUESTION_FORMAT_TYPE_DEF>
  <QUESTION_FORMAT_TYPE_DEF PARENT="U" CHILDREN="$" LABEL="Unique Other">U-O</QUESTION_FORMAT_TYPE_DEF>
</QUESTION_FORMAT_TYPE_SET_DEF>
```

### 3. IR Results XML format ###
An example of "ir_result.xml" is below.
```
#!xml
<?xml version="1.0" encoding="UTF-8"?>
<IR_RESULT_SET>
  <IR_RESULT TOPIC_ID="A1" QUERY_ID="1" FILE_NAME="(IF YOU USE A QUESTION_ANALYSIS_RESULT, PLEASE WRITE THE FILE NAME. OTHERWISE DO NOT WRITE ANYTHING)">
    <QUERY>
      <KEY_TERM_SET LANGUAGE="JA">
        <KEY_TERM RANK="1" SCORE="1.0">ファタハ</KEY_TERM>
        <KEY_TERM RANK="2" SCORE="0.1">組織</KEY_TERM>
      </KEY_TERM_SET>
    </QUERY>
    <DOCUMENT_SET>
      <DOCUMENT RANK="1" SCORE="0.5342" SOURCE_ID="https://ja.wikipedia.org/wiki/%E3%83%95%E3%82%A1%E3%82%BF%E3%83%8F" SOURCE_ID_TYPE="WEB">
        <![CDATA[text1]]>
      </DOCUMENT>
      <DOCUMENT RANK="2" SCORE="0.2423" SOURCE_ID="WA-24" SOURCE_ID_TYPE="QALAB2">
        <![CDATA[text2]]>
      </DOCUMENT>
      <DOCUMENT RANK="3" SCORE="0.2236" SOURCE_ID="(DOCUMENT_ID)" SOURCE_ID_TYPE="PARTICIPANT">
        <![CDATA[text3]]>
      </DOCUMENT>
    </DOCUMENT_SET>
  </IR_RESULT>
</IR_RESULT_SET>
```

* RANK attribute has an integer of 1 or more. Two nodes which have the same parent node are not allowed to have the same rank.
* SCORE attribute has any value between and including 0 and 1, higher is better.
* DOCUMENT_SET node has up to 50 DOCUMENT nodes.
* DOCUMENT node has up to 300 characters in Japanese or up to 150 words in English.
* FILE_NAME attribute has a file name of a QA result that you receive. If you receive a question XML, it is empty.
* QUERY_ID attribute has a query id in a QA result. If you receive a question XML, it is empty.
* SOURCE_ID attribute has a URL or a DOCUMENT_ID
* SOURCE_ID_TYPE attribute has one of the following types:
    * WEB (search from Web)
    * QALAB2 (documents delivered by QA-Lab2)
    * PARTICIPANT (documents prepared by each participating team, please describe in the system description form)